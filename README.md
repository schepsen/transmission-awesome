## PROJECT ##

* ID: **T**ransmission!**A**WESOME
* Contact: nikolaj@schepsen.eu

## PREREQUISITES ##

On Ubuntu, you can install the required development tools with this command:

```
sudo apt-get install build-essential automake autoconf libtool pkg-config intltool libcurl4-openssl-dev libglib2.0-dev libevent-dev libminiupnpc-dev libgtk-3-dev libappindicator3-dev
```

## BUILDING Transmission!AWESOME ##

* Create a build directory
* Go into
* Configure the Project (e.g.)

```
../src/configure --enable-lightweight --disable-daemon --disable-cli
```

* Run make to compile the latest version
* ...
* Profit!

## WHAT IS THAT FOR AN APPLICATION? ##

**T**ransmission!**A**WESOME is a bit modified bittorrent client based on Transmission

## PURPOSES ##

Allow the user rejects all peer's requests and sends a modified ratio to the server

## CHANGELOG ##

### Transmission!AWESOME 1.0, updated @ 2019-10-22 ###

* Added "Transmission!GHOSTSEED" beta version

    * allows you to seed without any local files
    * simulates the download process; just change the file extention to ".gs"
    * build with make CFLAGS=-DGHOSTSEED

 Why?! Lets assume you want to earn some seedtime bonuses, but you've neither time, bandwidth nor space

### Transmission!AWESOME 1.0, updated @ 2019-10-15 ###

* Switched to a new faking ratio strategy (Dx1.0, Ux1.0 + rand(2)))

### Transmission!AWESOME 1.0, updated @ 2018-05-20 ###

* Upgraded Transmission to the version 2.94

### Transmission!AWESOME 1.0, updated @ 2018-02-27 ###

* Upgraded Transmission to the version 2.93

### Transmission!AWESOME 1.0, updated @ 2016-03-07 ###

* Upgraded Transmission to the version 2.92
* Added a MOD rejects any request

### Transmission!AWESOME 1.0, updated @ 2016-03-05 ###

* Upgraded Transmission to the version 2.90

### Transmission!AWESOME 1.0, updated @ 2016-02-16 ###

* Initial release (based on Transmission 2.84)
